#include "ActorsSerializer.h"

#if WITH_EDITOR
#include "Editor/PropertyEditor/Public/PropertyEditorModule.h"
#include "ActorDetailsCustomization.h"
#endif


IMPLEMENT_PRIMARY_GAME_MODULE(FActorsSerializerModule, ActorsSerializer, "ActorsSerializer" );

void FActorsSerializerModule::StartupModule()
{
	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomClassLayout("Actor", FOnGetDetailCustomizationInstance::CreateStatic(&FActorDetailsCustomization::MakeInstance));

	UE_LOG(LogTemp, Warning, TEXT("Actors Serializer module is started!"));
}

void FActorsSerializerModule::ShutdownModule()
{
	UE_LOG(LogTemp, Warning, TEXT("Actors Serializer module is started!"));
}