// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FActorsSerializerModule : public IModuleInterface
{
public:
	void StartupModule() override;
	void ShutdownModule() override;
	bool IsGameModule() const override { return true; }
};