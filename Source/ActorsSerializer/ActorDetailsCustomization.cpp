#include "ActorDetailsCustomization.h"

#if WITH_EDITOR
#define LOCTEXT_NAMESPACE "ActorDetailsCustomization"

#include "Editor/PropertyEditor/Public/DetailLayoutBuilder.h"
#include "Editor/PropertyEditor/Public/DetailCategoryBuilder.h"
#include "Editor/PropertyEditor/Public/DetailWidgetRow.h"
#include "Runtime/Slate/Public/Widgets/Text/STextBlock.h"
#include "Runtime/Slate/Public/Widgets/Input/SButton.h"

#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/GameModeBase.h"
#include "Components/TextRenderComponent.h"
#include "Internationalization/TextLocalizationResource.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"

namespace
{
	const FString FILE_PATH = FPaths::GetProjectFilePath() + "/../ActorsInfo/SerializedActorsInfo.csv";
	const FName COMPONENT_TAG = "ACTOR_SERIALIZATION_COMPONENT";
}

TSharedRef<IDetailCustomization> FActorDetailsCustomization::MakeInstance()
{
	return MakeShareable(new FActorDetailsCustomization);
}

void FActorDetailsCustomization::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	IDetailCategoryBuilder& Category = DetailBuilder.EditCategory("Actor Serialization Properties", FText::GetEmpty(), ECategoryPriority::Important);
	TargetActorObjects.Empty();
	DetailBuilder.GetObjectsBeingCustomized(TargetActorObjects);
	ReadFile();
	UpdateCheckboxState();

	Category.AddCustomRow(LOCTEXT("RowSearchName", "ActorSerialization"))
		.NameContent()
		[
			SNew(STextBlock)
			.Text(LOCTEXT("ActorSerialization", "Toggle Actor Serialization"))
			.Font(IDetailLayoutBuilder::GetDetailFont())
		]
		.ValueContent()
		[
			SNew(SCheckBox)
			.IsChecked(this, &FActorDetailsCustomization::GetCheckBoxState)
			.OnCheckStateChanged(this, &FActorDetailsCustomization::OnActorSerializationCheckboxToggled)
		];
}

ECheckBoxState FActorDetailsCustomization::GetCheckBoxState() const
{
	return IsCheckboxEnabled ? ECheckBoxState::Checked : ECheckBoxState::Unchecked;
}

void FActorDetailsCustomization::OnActorSerializationCheckboxToggled(ECheckBoxState NewCheckedState)
{
	AActor* TargetActor = nullptr;
	int Hash = 0;
	FString Name = "";

	for (const auto& Object : TargetActorObjects)
	{
		TargetActor = GetTargetActorInfo(Object, Hash, Name);

		if (TargetActor)
		{
			const bool IsActorSerialized = SerializedActorsInfo.Contains(Hash);

			if (IsActorSerialized && NewCheckedState == ECheckBoxState::Unchecked)
			{
				SerializedActorsInfo.Remove(Hash);
				RemoveVisualizedIdComponentFromActor(TargetActor);
				WriteFile();
			}
			else if (!IsActorSerialized && NewCheckedState == ECheckBoxState::Checked)
			{
				SerializedActorsInfo.Emplace(Hash, Name);
				AddVisualizedIdComponentToActor(TargetActor, Hash);
				WriteFile();
			}
		}
	}

	UpdateCheckboxState();
}

void FActorDetailsCustomization::UpdateCheckboxState()
{
	IsCheckboxEnabled = false;

	if (!TargetActorObjects.Num())
		return;

	for (const auto& Object : TargetActorObjects)
	{
		int Hash = 0;
		FString Name = "";
		if (!GetTargetActorInfo(Object, Hash, Name) || !SerializedActorsInfo.Find(Hash))
			return;
	}

	IsCheckboxEnabled = true;
}

AActor* FActorDetailsCustomization::GetTargetActorInfo(const TWeakObjectPtr<UObject>& Object, int& Hash, FString& Name) const
{
	if (Object.IsValid())
	{
		AActor* OutActor = Cast<AActor>(Object);
		if (OutActor)
		{
			OutActor->GetName(Name);
			Hash = FTextLocalizationResource::HashString(FString(OutActor->GetLevel()->GetOuter()->GetName() + Name));
			return OutActor;
		}
	}

	return nullptr;
}

void FActorDetailsCustomization::AddVisualizedIdComponentToActor(AActor* Actor, const int Id)
{
	if (!Actor)
		return;

	// create component
	UTextRenderComponent* TextRenderComponent = NewObject<UTextRenderComponent>(Actor, "ActorSerializationIdComponent");
	Actor->AddInstanceComponent(TextRenderComponent);
	FAttachmentTransformRules Rules(EAttachmentRule::KeepRelative, false);
	TextRenderComponent->ComponentTags.Add(COMPONENT_TAG);
	TextRenderComponent->AttachToComponent(Actor->GetRootComponent(), Rules);
	TextRenderComponent->RegisterComponent();
	TextRenderComponent->SetText(FString("ID: " + FString::FromInt(Id)));
	TextRenderComponent->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);

	// locate above bounds
	const float AdditionalZOffset = 10.f;
	const FVector ComponentLocationOffset = FVector(0.f, 0.f, Actor->GetComponentsBoundingBox(true)[1].Z + AdditionalZOffset);
	TextRenderComponent->SetRelativeLocation(TextRenderComponent->GetRelativeLocation() + ComponentLocationOffset);
}

void FActorDetailsCustomization::RemoveVisualizedIdComponentFromActor(AActor* Actor)
{
	if (!Actor)
		return;

	const auto& Components = Actor->GetComponentsByTag(UTextRenderComponent::StaticClass(), COMPONENT_TAG);
	for (auto Component : Components)
	{
		Actor->RemoveInstanceComponent(Component);
		Component->DestroyComponent();
		Component = nullptr;
	}
}

void FActorDetailsCustomization::WriteFile()
{
	FString Info = "";
	for (const auto& ActorInfo : SerializedActorsInfo)
		Info += FString::FromInt(ActorInfo.Key) + "," + ActorInfo.Value + LINE_TERMINATOR;

	FFileHelper::SaveStringToFile(Info, *FILE_PATH);
}

void FActorDetailsCustomization::ReadFile()
{
	TArray<FString> FileRawData;
	FFileHelper::LoadFileToStringArray(FileRawData, *FILE_PATH);
	SerializedActorsInfo.Empty();

	for (const auto& FullRow : FileRawData)
	{
		TArray<FString> RowArray = {};
		FullRow.ParseIntoArray(RowArray, TEXT(","), false);
		SerializedActorsInfo.Emplace(FCString::Atoi(*RowArray[0]), RowArray[1]);
	}
}

#undef LOCTEXT_NAMESPACE
#endif