#pragma once

#if WITH_EDITOR

#include "Editor/DetailCustomizations/Public/DetailCustomizations.h"
#include "Editor/PropertyEditor/Public/IDetailCustomization.h"

class FActorDetailsCustomization : public IDetailCustomization
{
public:
	using ActorsInfo_t = TMap<int,FString>;

	static TSharedRef<IDetailCustomization> MakeInstance();
	void CustomizeDetails(IDetailLayoutBuilder&) override;

private:
	ECheckBoxState GetCheckBoxState() const;
	void OnActorSerializationCheckboxToggled(ECheckBoxState);
	void UpdateCheckboxState();
	AActor* GetTargetActorInfo(const TWeakObjectPtr<UObject>&, int&, FString&) const;
	void AddVisualizedIdComponentToActor(AActor*, const int);
	void RemoveVisualizedIdComponentFromActor(AActor*);

	void WriteFile();
	void ReadFile();

private:
	TArray<TWeakObjectPtr<UObject>> TargetActorObjects;
	ActorsInfo_t SerializedActorsInfo;
	bool IsCheckboxEnabled;
};

#endif
