// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DetailsActor.generated.h"

UCLASS()
class ACTORSSERIALIZER_API ADetailsActor : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "TestCat")
	FString CoolStringValue;

};
