// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class ActorsSerializer : ModuleRules
{
	public ActorsSerializer(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		if (Target.bBuildEditor)
		{
			PrivateDependencyModuleNames.AddRange(new string[] { "DetailCustomizations", "PropertyEditor", "EditorStyle" });
			PublicDependencyModuleNames.AddRange(new string[] { "UnrealEd" });
		}
	}
}
